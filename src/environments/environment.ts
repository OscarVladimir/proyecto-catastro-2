// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCAqMPHQDKxYvz-QrG2xDphgILVVC2jneo",
    authDomain: "bookstore-c831e.firebaseapp.com",
    databaseURL: "https://bookstore-c831e.firebaseio.com",
    projectId: "bookstore-c831e",
    storageBucket: "bookstore-c831e.appspot.com",
    messagingSenderId: "912867579767",
    appId: "1:912867579767:web:4df6e0ca80e389f8"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
